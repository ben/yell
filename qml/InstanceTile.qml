import QtQuick 2.12
import QtQuick.Window 2.12
import Yell.Model 1.0

Rectangle {
    id: instanceTile
    width: 480
    height: 100
    border.width: 0

    property InstanceModel instance: InstanceModel {
    }

    property int numAccounts: 0

    Image {
        id: icon
        width: 100
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        source: instance.icon
        fillMode: Image.PreserveAspectCrop
    }

    Text {
        id: name
        text: instance.name
        anchors.top: parent.top
        anchors.topMargin: 6
        anchors.right: softwareIcon.left
        anchors.rightMargin: 6
        font.weight: Font.Bold
        anchors.left: icon.right
        anchors.leftMargin: 6
        font.pixelSize: 24
    }

    Text {
        id: domain
        text: instance.domain
        anchors.right: softwareIcon.left
        anchors.rightMargin: 6
        anchors.left: icon.right
        anchors.leftMargin: 6
        anchors.top: name.bottom
        anchors.topMargin: 6
        font.pixelSize: 16
    }

    Image {
        id: softwareIcon
        width: 64
        height: 64
        sourceSize.height: 64 * ScreenInfo.pixelDensity
        sourceSize.width: 64 * ScreenInfo.pixelDensity
        anchors.top: parent.top
        anchors.topMargin: 6
        anchors.right: parent.right
        anchors.rightMargin: 6
        source: instance.software.icon
    }

    Text {
        id: element
        text: qsTr("%n accounts", "0", numAccounts)
        anchors.left: icon.right
        anchors.leftMargin: 6
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 6
        anchors.right: parent.right
        anchors.rightMargin: 6
        font.pixelSize: 16
    }
}
