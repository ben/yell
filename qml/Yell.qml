import QtQuick 2.12
import QtQuick.Controls 2.12
import "."

ApplicationWindow {
    id: mainWindow
    width: 480
    height: 100
    visible: true
    property alias instanceTile: instanceTile
    title: qsTr("Yell")

    InstanceTile {
        id: instanceTile
    }
}
