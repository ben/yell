package main

import (
	"os"

	_ "git.lubar.me/ben/yell/model"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/qml"
)

func main() {
	// Make the app look nicer on high-DPI screens
	core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)

	// Needs to be called before using QML
	app := gui.NewQGuiApplication(len(os.Args), os.Args)

	engine := qml.NewQQmlApplicationEngine(app)
	engine.Load(core.NewQUrl3("qrc:/qml/Yell.qml", 0))

	// Start main loop
	app.Exec()
}
