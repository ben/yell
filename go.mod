module git.lubar.me/ben/yell

go 1.12

require (
	github.com/donovanhide/eventsource v0.0.0-20171031113327-3ed64d21fb0b
	github.com/therecipe/qt v0.0.0-20190628021130-a9acd1ab63c1
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80
	golang.org/x/text v0.3.2
)
