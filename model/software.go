package model

import "github.com/therecipe/qt/core"

func init() {
	SoftwareModel_QmlRegisterType2("Yell.Model", 1, 0, "SoftwareModel")
}

var (
	Mastodon    = newSoftwareModel(core.QObject_Tr("Mastodon", "software name", -1), "qrc:/qml/logos/mastodon.svg")
	Pleroma     = newSoftwareModel(core.QObject_Tr("Pleroma", "software name", -1), "qrc:/qml/logos/pleroma.svg")
	Pixelfed    = newSoftwareModel(core.QObject_Tr("Pixelfed", "software name", -1), "qrc:/qml/logos/pixelfed.svg")
	PeerTube    = newSoftwareModel(core.QObject_Tr("PeerTube", "software name", -1), "qrc:/qml/logos/peertube.svg")
	WriteFreely = newSoftwareModel(core.QObject_Tr("WriteFreely", "software name", -1), "qrc:/qml/logos/writefreely.svg")
)

type SoftwareModel struct {
	core.QObject

	_ string     `property:"name"`
	_ *core.QUrl `property:"icon"`
}

func newSoftwareModel(name, icon string) *SoftwareModel {
	s := NewSoftwareModel(nil)
	s.SetName(name)
	s.SetIcon(core.NewQUrl3(icon, core.QUrl__StrictMode))
	return s
}
