package model

import "github.com/therecipe/qt/core"

func init() {
	InstanceModel_QmlRegisterType2("Yell.Model", 1, 0, "InstanceModel")
}

type InstanceModel struct {
	core.QObject

	_ func() `constructor:"initExample"`

	_ string         `property:"name"`
	_ string         `property:"domain"`
	_ *core.QUrl     `property:"icon"`
	_ *SoftwareModel `property:"software"`
}

func (i *InstanceModel) initExample() {
	i.SetName("Example Instance")
	i.SetDomain("social.example.net")
	i.SetIcon(core.NewQUrl3("qrc:/qml/placeholder.png", core.QUrl__StrictMode))
	i.SetSoftware(Mastodon)
}
