package mastodon

import (
	"math/big"
	"time"

	"golang.org/x/text/language"
)

type ID string

func (id ID) Cmp(other ID) int {
	if len(id) > len(other) {
		return 1
	}
	if len(id) < len(other) {
		return -1
	}

	if id > other {
		return 1
	}
	if id < other {
		return -1
	}

	return 0
}

type UnixTime int64

func (ut UnixTime) Time() time.Time {
	return time.Unix(int64(ut), 0).UTC()
}

func (ut *UnixTime) SetTime(t time.Time) {
	*ut = UnixTime(t.Unix())
}

// Type aliases declared for documentation
type (
	HTML = string
	URL  = string
)

type Account struct {
	ID             ID             `json:"id" since:"0.1.0"`
	Username       string         `json:"username" since:"0.1.0"`
	Acct           string         `json:"acct" since:"0.1.0"`
	DisplayName    string         `json:"display_name" since:"0.1.0"`
	Locked         bool           `json:"locked" since:"0.1.0"`
	CreatedAt      time.Time      `json:"created_at" since:"0.1.0"`
	FollowersCount int            `json:"followers_count" since:"0.1.0"`
	FollowingCount int            `json:"following_count" since:"0.1.0"`
	StatusesCount  int            `json:"statuses_count" since:"0.1.0"`
	Note           string         `json:"note" since:"0.1.0"`
	URL            URL            `json:"url" since:"0.1.0"`
	Avatar         URL            `json:"avatar" since:"0.1.0"`
	AvatarStatic   URL            `json:"avatar_static" since:"1.1.2"`
	Header         URL            `json:"header" since:"0.1.0"`
	HeaderStatic   URL            `json:"header_static" since:"1.1.2"`
	Emojis         []Emoji        `json:"emojis" since:"2.4.0"`
	Moved          *Account       `json:"moved" since:"2.1.0"`
	Fields         []AccountField `json:"fields" since:"2.4.0"`
	Bot            *bool          `json:"bot" since:"2.4.0"`
}

type AccountField struct {
	Name       string     `json:"name" since:"2.4.0"`
	Value      HTML       `json:"value" since:"2.4.0"`
	VerifiedAt *time.Time `json:"verified_at" since:"2.6.0"`
}

type AccountSource struct {
	Privacy   *string        `json:"privacy" since:"1.5.0"`
	Sensitive *bool          `json:"sensitive" since:"1.5.0"`
	Language  *language.Tag  `json:"language" since:"2.4.2"`
	Note      string         `json:"note" since:"1.5.0"`
	Fields    []AccountField `json:"fields" since:"2.4.0"`
}

type AccountToken struct {
	AccessToken string   `json:"access_token" since:"0.1.0"`
	TokenType   string   `json:"token_type" since:"0.1.0"`
	Scope       string   `json:"scope" since:"0.1.0"`
	CreatedAt   UnixTime `json:"created_at" since:"0.1.0"`
}

type Application struct {
	Name    string `json:"name" since:"0.9.9"`
	Website *URL   `json:"website" since:"0.9.9"`
}

type AttachmentType string

const (
	AttachmentUnknown AttachmentType = "unknown"
	AttachmentImage   AttachmentType = "image"
	AttachmentGifv    AttachmentType = "gifv"
	AttachmentVideo   AttachmentType = "video"
)

type AttachmentMeta struct {
	Small    *AttachmentMetaData `json:"small"`
	Original *AttachmentMetaData `json:"original"`
	Focus    *AttachmentFocus    `json:"focus"`
}

type AttachmentMetaData struct {
	// shared
	Width  *int `json:"width"`
	Height *int `json:"height"`

	// images
	Size   *string  `json:"size"`
	Aspect *float64 `json:"aspect"`

	// videos
	FrameRate *string  `json:"frame_rate"`
	Duration  *float64 `json:"duration"`
	Bitrate   *int     `json:"bitrate"`
}

type AttachmentFocus struct {
	X *float64 `json:"x"`
	Y *float64 `json:"y"`
}

type Attachment struct {
	ID          ID              `json:"id" since:"0.6.0"`
	Type        AttachmentType  `json:"type" since:"0.6.0"`
	URL         URL             `json:"url" since:"0.6.0"`
	RemoteURL   *URL            `json:"remote_url" since:"0.6.0"`
	PreviewURL  URL             `json:"preview_url" since:"0.6.0"`
	TextURL     *URL            `json:"text_url" since:"0.6.0"`
	Meta        *AttachmentMeta `json:"meta" since:"1.5.0"`
	Description *string         `json:"description" since:"2.0.0"`
}

type CardType string

const (
	CardLink  CardType = "link"
	CardPhoto CardType = "photo"
	CardVideo CardType = "video"
	CardRich  CardType = "rich"
)

type Card struct {
	URL          URL      `json:"url" since:"1.0.0"`
	Title        string   `json:"title" since:"1.0.0"`
	Description  string   `json:"description" since:"1.0.0"`
	Image        *URL     `json:"image" since:"1.0.0"`
	Type         CardType `json:"type" since:"1.3.0"`
	AuthorName   *string  `json:"author_name" since:"1.3.0"`
	AuthorURL    *URL     `json:"author_url" since:"1.3.0"`
	ProviderName *string  `json:"provider_name" since:"1.3.0"`
	ProviderURL  *URL     `json:"provider_url" since:"1.3.0"`
	Width        *int     `json:"width" since:"1.3.0"`
	Height       *int     `json:"height" since:"1.3.0"`
}

type Context struct {
	Ancestors   []Status `json:"ancestors" since:"0.6.0"`
	Descendants []Status `json:"descendants" since:"0.6.0"`
}

type Emoji struct {
	Shortcode       string `json:"shortcode" since:"2.0.0"`
	StaticURL       URL    `json:"static_url" since:"2.0.0"`
	URL             URL    `json:"url" since:"2.0.0"`
	VisibleInPicker bool   `json:"visible_in_picker" since:"2.1.0"`
}

type Error struct {
	// HTTP status code
	Status int `json:"-"`

	Message string `json:"error" since:"0.6.0"`
}

func (err Error) Error() string {
	return err.Message
}

type FilterContext string

const (
	FilterHome          FilterContext = "home"
	FilterNotifications FilterContext = "notifications"
	FilterPublic        FilterContext = "public"
	FilterThread        FilterContext = "thread"
)

/*

If whole_word is true , client app should do:

- Define ‘word constituent character’ for your app. In the official implementation, it’s [A-Za-z0-9_] in JavaScript, and [[:word:]] in Ruby. In Ruby case it’s the POSIX character class (Letter | Mark | Decimal_Number | Connector_Punctuation).
- If the phrase starts with a word character, and if the previous character before matched range is a word character, its matched range should be treated to not match.
- If the phrase ends with a word character, and if the next character after matched range is a word character, its matched range should be treated to not match.

Please check app/javascript/mastodon/selectors/index.js and app/lib/feed_manager.rb in the Mastodon source code for more details.

*/

type Filter struct {
	ID           ID              `json:"id" since:"2.4.3"`
	Phrase       string          `json:"phrase" since:"2.4.3"`
	Context      []FilterContext `json:"context" since:"2.4.3"`
	ExpiresAt    *time.Time      `json:"expires_at" since:"2.4.3"`
	Irreversible bool            `json:"irreversible" since:"2.4.3"`
	WholeWorld   bool            `json:"whole_word" since:"2.4.3"`
}

type Instance struct {
	URI            string         `json:"uri" since:"1.1.0"`
	Title          string         `json:"title" since:"1.1.0"`
	Description    string         `json:"description" since:"1.1.0"`
	Email          string         `json:"email" since:"1.1.0"`
	Version        string         `json:"version" since:"1.3.0"`
	Thumbnail      *URL           `json:"thumbnail" since:"1.6.1"`
	URLs           InstanceURLs   `json:"urls" since:"1.4.2"`
	Stats          InstanceStats  `json:"stats" since:"1.6.0"`
	Languages      []language.Tag `json:"languages" since:"2.3.0"`
	ContactAccount *Account       `json:"contact_account" since:"2.3.0"`
}

type InstanceURLs struct {
	StreamingAPI URL `json:"streaming_api" since:"1.4.2"`
}

type InstanceStats struct {
	UserCount   *big.Int `json:"user_count" since:"1.6.0"`
	StatusCount *big.Int `json:"status_count" since:"1.6.0"`
	DomainCount *big.Int `json:"domain_count" since:"1.6.0"`
}

type List struct {
	ID    ID     `json:"id" since:"2.1.0"`
	Title string `json:"title" since:"2.1.0"`
}

type Mention struct {
	URL      URL    `json:"url" since:"0.6.0"`
	Username string `json:"username" since:"0.6.0"`
	Acct     string `json:"acct" since:"0.6.0"`
	ID       ID     `json:"id" since:"0.6.0"`
}

type NotificationType string

const (
	NotificationFollow    NotificationType = "follow"
	NotificationMention   NotificationType = "mention"
	NotificationReblog    NotificationType = "reblog"
	NotificationFavourite NotificationType = "favourite"
)

type Notification struct {
	ID        ID               `json:"id" since:"0.9.9"`
	Type      NotificationType `json:"type" since:"0.9.9"`
	CreatedAt time.Time        `json:"created_at" since:"0.9.9"`
	Account   Account          `json:"account" since:"0.9.9"`
	Status    *Status          `json:"status" since:"0.9.9"`
}

type Poll struct {
	ID         ID           `json:"id" since:"2.8.0"`
	ExpiresAt  *time.Time   `json:"expires_at" since:"2.8.0"`
	Expired    bool         `json:"expired" since:"2.8.0"`
	Multiple   bool         `json:"multiple" since:"2.8.0"`
	VotesCount int          `json:"votes_count" since:"2.8.0"`
	Options    []PollOption `json:"options" since:"2.8.0"`
	Voted      *bool        `json:"voted" since:"2.8.0"`
}

type PollOption struct {
	Title      string `json:"title" since:"2.8.0"`
	VotesCount *int   `json:"votes_count" since:"2.8.0"`
}

type PushSubscriptionAlerts map[string]interface{} // documentation is simply "???"

type PushSubscription struct {
	ID        ID                     `json:"id" since:"2.4.0"`
	Endpoint  URL                    `json:"endpoint" since:"2.4.0"`
	ServerKey string                 `json:"server_key" since:"2.4.0"`
	Alerts    PushSubscriptionAlerts `json:"alerts" since:"2.4.0"`
}

type Relationhip struct {
	ID                  ID   `json:"id" since:"0.6.0"`
	Following           bool `json:"following" since:"0.6.0"`
	FollowedBy          bool `json:"followed_by" since:"0.6.0"`
	Blocking            bool `json:"blocking" since:"0.6.0"`
	Muting              bool `json:"muting" since:"1.1.0"`
	MutingNotifications bool `json:"muting_notifications" since:"2.1.0"`
	Requested           bool `json:"requested" since:"0.9.9"`
	DomainBlocking      bool `json:"domain_blocking" since:"1.4.0"`
	ShowingReblogs      bool `json:"showing_reblogs" since:"2.1.0"`
	Endorsed            bool `json:"endorsed" since:"2.5.0"`
}

type Results struct {
	Accounts []Account `json:"accounts" since:"1.1.0"`
	Statuses []Status  `json:"statuses" since:"1.1.0"`
	Hashtags []Tag     `json:"hashtags" since:"1.1.0"`
}

type Status struct {
	ID                 ID            `json:"id" since:"0.1.0"`
	URI                string        `json:"uri" since:"0.1.0"`
	URL                *URL          `json:"url" since:"0.1.0"`
	Account            Account       `json:"account" since:"0.1.0"`
	InReplyToID        *ID           `json:"in_reply_to_id" since:"0.1.0"`
	InReplyToAccountID *ID           `json:"in_reply_to_account_id" since:"1.0.0"`
	Reblog             *Status       `json:"reblog" since:"0.1.0"`
	Content            HTML          `json:"content" since:"0.1.0"`
	CreatedAt          time.Time     `json:"created_at" since:"0.1.0"`
	Emojis             []Emoji       `json:"emojis" since:"2.0.0"`
	RepliesCount       int           `json:"replies_count" since:"2.5.0"`
	ReblogsCount       int           `json:"reblogs_count" since:"0.1.0"`
	FavouritesCount    int           `json:"favourites_count" since:"0.1.0"`
	Reblogged          *bool         `json:"reblogged" since:"0.1.0"`
	Favourited         *bool         `json:"favourited" since:"0.1.0"`
	Muted              *bool         `json:"muted" since:"1.4.0"`
	Sensitive          bool          `json:"sensitive" since:"0.9.9"`
	SpoilerText        string        `json:"spoiler_text" since:"1.0.0"`
	Visibility         Visibility    `json:"visibility" since:"0.9.9"`
	MediaAttachments   []Attachment  `json:"media_attachments" since:"0.6.0"`
	Mentions           []Mention     `json:"mentions" since:"0.6.0"`
	Tags               []Tag         `json:"tags" since:"0.9.0"`
	Card               *Card         `json:"card" since:"2.6.0"`
	Poll               *Poll         `json:"poll" since:"2.8.0"`
	Application        Application   `json:"application" since:"0.9.9"`
	Language           *language.Tag `json:"language" since:"1.4.0"`
	Pinned             *bool         `json:"pinned" since:"1.6.0"`
}

type Visibility string

const (
	VisibilityPublic        Visibility = "public"
	VisibilityUnlisted      Visibility = "unlisted"
	VisibilityFollowersOnly Visibility = "private"
	VisibilityDirect        Visibility = "direct"
)

type ScheduledStatus struct {
	ID               ID           `json:"id" since:"2.7.0"`
	ScheduledAt      time.Time    `json:"scheduled_at" since:"2.7.0"`
	Params           StatusParams `json:"params" since:"2.7.0"`
	MediaAttachments []Attachment `json:"media_attachments" since:"2.7.0"`
}

type StatusParams struct {
	Text          string     `json:"text" since:"2.7.0"`
	InReplyToID   *ID        `json:"in_reply_to_id" since:"2.7.0"`
	MediaIDs      []ID       `json:"media_ids" since:"2.7.0"`
	Sensitive     *bool      `json:"sensitive" since:"2.7.0"`
	SpoilerText   *string    `json:"spoiler_text" since:"2.7.0"`
	Visibility    Visibility `json:"visibility" since:"2.7.0"`
	ScheduledAt   *time.Time `json:"scheduled_at" since:"2.7.0"`
	ApplicationID ID         `json:"application_id" since:"2.7.0"`
}

type Tag struct {
	Name    string       `json:"name" since:"0.9.0"`
	URL     URL          `json:"url" since:"0.9.0"`
	History []TagHistory `json:"history" since:"2.4.1"`
}

type TagHistory struct {
	Day      UnixTime `json:"day,string" since:"2.4.1"`
	Uses     int      `json:"uses" since:"2.4.1"`
	Accounts int      `json:"accounts" since:"2.4.1"`
}

type Conversation struct {
	ID         ID        `json:"id" since:"2.6.0"`
	Accounts   []Account `json:"accounts" since:"2.6.0"`
	LastStatus *Status   `json:"last_status" since:"2.6.0"`
	Unread     bool      `json:"unread" since:"2.6.0"`
}
