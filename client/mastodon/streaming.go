package mastodon

import (
	"encoding/json"
	"net/url"
	"strconv"

	"github.com/donovanhide/eventsource"
	"golang.org/x/net/websocket"
)

type StreamingAPI interface {
	User(notificationsOnly bool) *Stream
	Federated(onlyMedia bool) *Stream
	Local(onlyMedia bool) *Stream
	Hashtag(tag string, local bool) *Stream
	List(id ID) *Stream
	Direct() *Stream
}

type streamingAPI struct {
	init func(*Stream)
	sep  string
}

func (s *streamingAPI) User(notificationsOnly bool) *Stream {
	base := "user"
	if notificationsOnly {
		base += s.sep + "notification"
	}
	return s.startStream(base, "")
}

func (s *streamingAPI) Federated(onlyMedia bool) *Stream {
	return s.startStream("public", "?only_media="+strconv.FormatBool(onlyMedia))
}

func (s *streamingAPI) Local(onlyMedia bool) *Stream {
	return s.startStream("public"+s.sep+"local", "?only_media="+strconv.FormatBool(onlyMedia))
}

func (s *streamingAPI) Hashtag(tag string, local bool) *Stream {
	base := "hashtag"
	if local {
		base += s.sep + "local"
	}
	return s.startStream(base, "?tag="+url.QueryEscape(tag))
}

func (s *streamingAPI) List(id ID) *Stream {
	return s.startStream("list", "?list="+url.QueryEscape(string(id)))
}

func (s *streamingAPI) Direct() *Stream {
	return s.startStream("direct", "")
}

func (s *streamingAPI) startStream(path, query string) *Stream {
	stream := &Stream{
		ch:     make(chan StreamingEvent, 10),
		close:  make(chan chan error),
		closed: make(chan struct{}),
		path:   path,
		query:  query,
		init:   s.init,
	}
	go stream.init(stream)

	return stream
}

type Stream struct {
	ch     chan StreamingEvent
	close  chan chan error
	closed chan struct{}
	path   string
	query  string
	init   func(*Stream)
}

func (s *Stream) Events() <-chan StreamingEvent {
	return s.ch
}

func (s *Stream) Closed() <-chan struct{} {
	return s.closed
}

func (s *Stream) Close() error {
	ch := make(chan error, 1)
	select {
	case s.close <- ch:
		err := <-ch
		close(s.closed)
		return err
	case <-s.closed:
		return nil
	}
}

func (s *Stream) push(event StreamingEventType, payload json.RawMessage) {
	e := StreamingEvent{
		Type:    event,
		Payload: payload,
	}
	switch event {
	case StreamingUpdate:
		e.Err = json.Unmarshal(payload, &e.Status)
	case StreamingNotification:
		e.Err = json.Unmarshal(payload, &e.Notification)
	case StreamingDelete:
		e.Err = json.Unmarshal(payload, &e.DeletedID)
	}

	s.ch <- e
}

func (s *Stream) fatal(err error) {
	func() {
		// "safely" close the channel
		defer func() {
			recover()
		}()
		close(s.closed)
	}()

	s.ch <- StreamingEvent{
		Err:   err,
		Fatal: true,
	}
}

func (s *Stream) reconnect(err error) {
	go s.init(s)

	s.ch <- StreamingEvent{
		Err: err,
	}
}

type serverSentEventsAPI struct {
	client *Client
}

func (sse *serverSentEventsAPI) initStream(s *Stream) {
	_ = "/api/v1/streaming/" + s.path + s.query
	_ = eventsource.NewDecoder
	panic("TODO")
}

type websocketAPI struct {
	client *Client
}

func (ws *websocketAPI) initStream(s *Stream) {
	path, query := s.path, s.query
	if query == "?only_media=false" {
		query = ""
	} else if query == "?only_media=true" {
		query = ""
		path += ":media"
	}
	if query == "" {
		query = "?"
	} else {
		query += "&"
	}
	query += "stream=" + url.QueryEscape(path)
	_ = "/api/v1/streaming" + query
	_ = websocket.Dial
	panic("TODO")
}

type StreamingEventType string

const (
	// A new status has appeared.
	StreamingUpdate StreamingEventType = "update"

	// A new notification has appeared.
	StreamingNotification StreamingEventType = "notification"

	// A status has been deleted.
	StreamingDelete StreamingEventType = "delete"

	// Keyword filters have been changed.
	// Payload format is undefined.
	StreamingFiltersChanged StreamingEventType = "filters_changed"
)

type StreamingEvent struct {
	// Err may be non-nil on any event type.
	Err error

	// If Fatal is true, the connection was closed due to Err.
	Fatal bool

	// If empty, this is an error-only event.
	Type StreamingEventType

	// For StreamingUpdate
	Status *Status

	// For StreamingNotification
	Notification *Notification

	// For StreamingDelete
	DeletedID ID

	// For unknown types
	Payload json.RawMessage
}
